\documentclass[10pt,journal, a4paper]{IEEEtran}
\usepackage[MeX]{polski}
\usepackage[utf8]{inputenc}  % Polskie
\usepackage[polish]{babel}     %
\usepackage{parskip}
\usepackage{latexsym}
\usepackage{graphicx}
\usepackage{url}
\usepackage{verbatim}
\usepackage{epstopdf}
\usepackage{cite}
\usepackage{ragged2e}
\usepackage{comment}
\usepackage{caption}
\setlength{\parindent}{1em}
\author{Anna Świeca \\Michał Karczewski \\ Piotr Stefański}
\title{Modelowanie i symulacja układu napędowego \\ oraz dynamiki robota kulistego}
\begin{document}
\maketitle
\begin{abstract}
W tym artykule przedstawione zostaną zagadnienia związane z dynamiką oraz sterowaniem robotem kulistym. Przedstawiony zostanie opis w postaci równań zawierający dynamikę ruchu robota. Sporządzony zostanie model oraz jego symulacja w środowisku MatLab/Simulink. Na tej podstawie zaproponowany zostanie regulator położenia robota w przestrzeni zmiennych stanu.
\end{abstract}
\begin{IEEEkeywords} %key words
Robot kulisty, dynamika, modelowanie, symulacja, regulator zmiennych stanu.
\end{IEEEkeywords}
\section{Wstęp}
\label{rozdzial:Wstep}


Ruch kulisty to jeden z podstawowych ruchów, z~którymi każdy inżynier powinien mieć do czynienia. Zagadnienie toczenia się kuli po płaskiej, równej powierzchni to zadanie interesujące pod względem zarówno mechaniki, jak i dynamiki. Konstrukcja robotów kulistych posiada wiele zalet. Jedną z nich jest cecha holonomiczności. Definiuje ona ograniczenia ruchu, jakim podlega robot. Mówi ona więc o możliwości robota do zmiany jego orientacji w miejscu. Daje to duże możliwości nawigacyjne. Pozwala również uniknąć sytuacji utknięcia w zakątkach, problemu zjazdu ze schodów, czy ruchu robota po jego upuszczeniu. 
Innymi zaletami robotów sferycznych może być ich budowa. Odpowiednia konstrukcja chroni wewnętrzne urządzenia (takie jak mikrokontroler, czy moduł regulatora) przed destrukcyjnym wpływem środowiska zewnętrznego. W szczególności, niektóre rozwiązania mogą zapewniać wodoszczelność. Projektując odpowiednio moduł ruchu - można uzyskać robota kulistego, który będzie miał możliwość poruszania się zarówno po lądzie, jak i~w~wodzie. 

\section{Przegląd obecnych rozwiązań}
\label{rozdzial:Przeglad_rozwiazan}

Przegląd literatury ma na celu przedłożyć, w jaki sposób może być realizowana idea robota w kształcie kuli, toczącego się po podłożu. 
\subsection{Roboty z kołem napędowym}
Główna koncepcja ich konstrukcji opiera się o tzw. IDU (\textit{ang. Inside Drive Unit})\cite{mainreview}. Polega to na umieszczeniu wewnątrz sfery układu napędowego wraz z jego zasilaniem, jednostką kontrolną oraz komunikacyjną. Tworzy się w ten sposób kompaktowe urządzenie mobilne, którego parametry zależą wyłącznie od parametrów własnych (nie uwzględniając tarcia). 
Najprostszą, a zarazem jedną z~najstarszych jest konstrukcja przedstawiona na rysunku~\ref{fig:01_simple_wheel}. 
\begin{figure}[!t]
\captionsetup{justification=centering}
\centering
\includegraphics[width=2.5in]{01_simple_wheel}
\caption{Struktura robota kulistego z jednym kołem napędowym.}
\label{fig:01_simple_wheel}
\end{figure}
Wewnątrz sfery~(1) robota znajduje się jego koło napędowe~(3) oraz oś sterująca~(4), które są kontrolowane za pomocą jednostki sterującej~(2). W~skład opisywanego mechanizmu wchodzą również elementy zapewniające utrzymanie równowagi - sprężyna~(6) i~balanser~(7). 
\begin{figure}[!ht]
\captionsetup{justification=centering}
\centering
\includegraphics[width=2.5in]{02_double_wheel}
\caption{Struktura robota kulistego z kołem omnikierunkowym.}
\label{fig:02_double_wheel}
\end{figure}
Rozbudowaną wersją robota z rysunku \ref{fig:01_simple_wheel} jest mechanizm posiadający tzw. koło szwedzkie (zwane również jako koło omnikierunkowe)\cite{omniwheel}. Przykładowy model 3D takiego robota przedstawia rysunek \ref{fig:02_double_wheel}. 

\subsection{Roboty z wahadłem jako elementem napędzającym}

Drugim sposobem napędzania robota kulistego jest wykorzystanie fizyki wahadła matematycznego. Najprostszym rozwiązaniem jest wykorzystanie bezwładnej masy, do której doczepione jest wahadło matematyczne (masa na nieważkim pręcie). Przykład takiego robota jest pokazany na rysunku \ref{fig:03_single_pendulum}\cite{mainreview}. Jego dynamika zostanie szczegółowo omówiona w rozdziale \ref{rozdzial:Dynamika}. 
\begin{figure}[!ht]
\captionsetup{justification=centering}
\centering
\includegraphics[width=2.5in]{03_single_pendulum}
\caption{Koncepcja robota z napędzającym wahadłem.}
\label{fig:03_single_pendulum}
\end{figure}

W literaturze można spotkać również konstrukcje bardziej zaawansowane, wykorzystujące do napędzania oraz zmiany orientacji cztery wahadła\cite{four_pendulum} umiejscowione tak, jak to jest pokazane w modelu na rysunku \ref{fig:04_four_pendulum}.
\begin{figure}[!ht]
\captionsetup{justification=centering}
\centering
\includegraphics[width=2.5in]{04_four_pendulum}
\caption{Model robota z czterema wahadłami.}
\label{fig:04_four_pendulum}
\end{figure}

\subsection{Roboty o różnej konstrukcji}

Oprócz powyższych dwóch grup konstrukcji robotów kulistych, które są dość powszechne, spotyka się w literaturze roboty o nietypowej budowie. Najczęściej są one budowane do przemieszczania się w ośrodku płynowym (np. woda) lub wykorzystujące niekonwencjonalny mechanizm napędowy. 

Przykładem robota przystosowanego do poruszania się wyłącznie w wodzie jest robot podwodny - rysunek \ref{fig:05_underwater}\cite{underwater}. 
\begin{figure}[!ht]
\captionsetup{justification=centering}
\centering
\includegraphics[width=2.5in]{05_underwater}
\caption{Model robota podwodnego.}
\label{fig:05_underwater}
\end{figure}

Ciekawym przykładem robota kulistego jest tzw. ,,amfibia". Robot ten nie należy do grupy IDU, gdyż do poruszania się po lądzie musi on odsłonić odnóża kroczące\cite{amfibia}. Na rysunku \ref{fig:06_amphibia} przedstawiony on został w dwóch trybach działania: (a) - tryb kroczenia po lądzie, oraz (b) - tryb przemieszczania się w wodzie. 
\begin{figure}[!ht]
\captionsetup{justification=centering}
\centering
\includegraphics[width=2.5in]{06_amphibia}
\caption{Model robota podwodnego.}
\label{fig:06_amphibia}
\end{figure}

Równie ciekawym przykładem niekonwencjonalnej budowy robota kulistego może być ,,RollRoller" (rysunek \ref{fig:07_rollroller}). Robot ten wewnątrz obudowy sferycznej posiada specjalnie ukształtowane rury, w których płynie ciecz. W~rurkach tych znajdują się również kulki (po jednej dla każdej rury), które służą jako masy bezwładne. Podczas generowania przepływu w rurze, kulka wprawiana jest w ruch, co powoduje wytworzenie siły bezwładności. To co z kolei skutkuje przemieszczeniem się robota\cite{rollroller}.
\begin{figure}[!ht]
\captionsetup{justification=centering}
\centering
\includegraphics[width=2.5in]{07_rollroller}
\caption{Model robota kulistego wykorzystującego do poruszania się prawa mechaniki płynów.}
\label{fig:07_rollroller}
\end{figure}
\section{Dynamika ruchu robota}
\label{rozdzial:Dynamika}

Procesowi modelowania zostanie poddany robot o konstrukcji przedstawionej na rysunku \ref{fig:08_dynamics}. Dla takiego układu zostaną wypisane równania Lagrange'a w celu uzyskania równań ruchu mechanizmu\cite{mainsource}.
\begin{figure}[!ht]
\captionsetup{justification=centering}
\centering
\includegraphics[width=2.5in]{08_dynamics}
\caption{Model robota, który zostanie poddany modelowaniu.}
\label{fig:08_dynamics}
\end{figure}
\setlength{\arraycolsep}{0.0em}
\begin{eqnarray}
U_1&{}={}&0, \label{eq:energy_1}\\
U_2&{}={}&-M_2 ge\cos(\theta_1+\theta_2), \\
K_1&{}={}&\frac{1}{2}M_1 (r\omega_1)^2, \\
K_2&{}={}&\frac{1}{2}M_2\Big( (r\omega_1 - e\cos(\theta_1+\theta_2)(\omega_1+\omega_2))^2\nonumber\\
&&+ (e\sin(\theta_1+\theta_2)(\omega_1+\omega_2))^2 \Big), \\
T_1&{}={}&\frac{1}{2}J_1\omega_1^2, \\
T_2&{}={}&\frac{1}{2}J_2(\omega_1+\omega_2)^2\label{eq:energy_2}.
\end{eqnarray}
\setlength{\arraycolsep}{5pt}
gdzie:\\
$U_1$ - energia potencjalna kuli w odniesieniu do jej środka, $U_2$ - energia potencjalna wahadła w odniesieniu do środka kuli, $K_1$ - energia kinetyczna kuli, $K_2$ - energia kinetyczna wahadła, $T_1$ - energia rotacji kuli, $T_2$ - energia rotacji wahadła, $r$ - promień kuli, $e$ - odległość między środkiem kuli a końcem wahadła, $\theta_1$ - kąt obrotu kuli, $\theta_2$ - kąt obrotu wahadła w odniesieniu do kuli, $\omega_1$ - prędkość kątowa kuli, $\omega_2$ - prędkość kątowa wahadła w odniesieniu do kuli, $J_1$ - moment bezwładności kuli względem jej środka, $J_2$ - moment bezwładności wahadła względem środka kuli, $M_1$ - masa kuli, $M_2$ - masa wahadła, $g$ - przyspieszenie ziemskie. 

Równanie Lagrange'a ma następującą postać:
\begin{equation}
L=K_1+K_2+T_1+T_2-U_1-U_2.
\end{equation}
Równania ruchu mają postać:
\setlength{\arraycolsep}{0.0em}
\begin{eqnarray}
\frac{d}{dt}\Big( \frac{\partial L}{\partial\omega_1} \Big)-\frac{\partial L}{\partial\theta_1}&{}={}&-T+T_f, \label{eq:lagrange_1}\\
\frac{d}{dt}\Big( \frac{\partial L}{\partial\omega_2} \Big)-\frac{\partial L}{\partial\theta_2}&{}={}&T\label{eq:lagrange_2}.
\end{eqnarray}
\setlength{\arraycolsep}{5pt}
gdzie:\\
$T$ - moment siły pomiędzy wahadłem, a kulą, \\
$T_f$ - moment pochodzący od siły tarcia: \\
\begin{equation}
T_f = T_c+T_v\omega_1,\label{eq:torque}
\end{equation}
gdzie: $T_c$ - współczynnik tarcia Coulombowskiego, $T_v$ - współczynnik tarcia wiskotycznego.\\
Wstawiając (\ref{eq:energy_1}-\ref{eq:energy_2}) do (\ref{eq:lagrange_1}) i (\ref{eq:lagrange_2}) mamy:
\setlength{\arraycolsep}{0.0em}
\begin{eqnarray}
-T+T_f&{}={}&a_1(J_1+J_2+M_1r^2+M_2r^2\nonumber\\
&&+M_2e^2-2M_2re\cos(\theta_1+\theta_2))\nonumber\\
&& +a_2(J_2-M_2re\cos(\theta_1+\theta_2)+M_2e^2)\nonumber\\
&& +M_2re\sin(\theta_1+\theta_2)(\omega_1+\omega_2)^2 \nonumber\\
&&+ M_2ge\sin(\theta_1+\theta_2)\label{eq:lagrange_second_1}, \\
T&{}={}&a_1(J_2-M_2re\cos(\theta_1+\theta_2)+M_2e^2)\nonumber\\
&&+a_2(J_2+M_2e^2)\nonumber\\
&&+M_2ge\sin(\theta_1+\theta_2)\label{eq:lagrange_second_2}.
\end{eqnarray}
\setlength{\arraycolsep}{5pt}

Należy teraz wprowadzić do układu urządzenie pobudzające wahadło. W tym celu zamodelowano silnik DC o~następujących równaniach:
\setlength{\arraycolsep}{0.0em}
\begin{eqnarray}
V&{}={}&I_t R_t+L_t \frac{d}{dt}I_t+c_v\omega, \label{eq:motor_voltage}\\
M_{em}-M_{obc}&{}={}&J_c \frac{d}{dt}\omega \label{eq:motor_torque}\\
M_{em}&{}={}&c_M I_t \label{eq:motor_noise},
\end{eqnarray}
\setlength{\arraycolsep}{5pt}

\begin{figure*}[!ht]
\captionsetup{justification=centering}
\centering
\includegraphics[width=\textwidth]{11_model_simulink_1}
\caption{Model robota kulistego}
\label{fig:11_model_simulink}
\end{figure*}
gdzie:\\
$V$ - napięcie przyłożone do zacisków silnika, $I_t$ - płynący przez silnik prąd, $R_t$ - rezystancja twornika silnika, $C_v$ - stała prędkościowa silnika, $\omega$ - prędkość obrotowa silnika, $M_{em}$ - moment wytwarzany przez silnik, $C_M$ - stała momentowa silnika, $M_{obc}$ - moment obciążenia, $J_c$ - moment bezwładności silnika.



Następnie należy zlinearyzować równania (\ref{eq:lagrange_second_1}) i (\ref{eq:lagrange_second_2}). Wartości wychylenia wahadła i kuli są niewielkie, dlatego $\theta_1+\theta_2\ll1$ oraz $\omega_1+\omega_2\ll1$.  Równania (\ref{eq:lagrange_second_1}) i (\ref{eq:lagrange_second_2}) po linearyzacji prezentują się następująco:
\setlength{\arraycolsep}{0.0em}
\begin{eqnarray}
-T+T_f&{}={}&a_1(J_1+J_2+M_1r^2+M_2r^2+M_2e^2\nonumber\\
&&-2M_2re)+a_2(J_2-M_2re+M_2e^2)\nonumber\\
&&+ M_2ge(\theta_1+\theta_2)\label{eq:lagrange_linear_1}, \\
T&{}={}&a_1(J_2-M_2re+M_2e^2)+a_2(J_2+M_2e^2)\nonumber\\
&&+M_2ge(\theta_1+\theta_2)\label{eq:lagrange_linear_2}.
\end{eqnarray}
\setlength{\arraycolsep}{5pt}

\section{Model robota}
\label{rozdzial:Model}


Model obiektu składać się będzie z dwóch części, modelu silnika DC oraz kuli z wahadłem. 
Model kuli z wahadłem otrzymujemy poprzez zastosowanie przekształcenia Laplace'a do równań (\ref{eq:lagrange_linear_1}) oraz (\ref{eq:lagrange_linear_2}), wyeliminowanie $T_f$ przy użyciu (\ref{eq:torque}) oraz pominięcie stałej $T_c$.
\setlength{\arraycolsep}{0.0em}
\begin{eqnarray}
-T&{}={}&[-sT_v+egM_2+s^2{J_1+J_2+r^2M_1+(e-r)^2M_2}]\theta_1\nonumber\\
&&+[s^2J_2+e{g+(e-r)s^2}M_2]\theta_2 \label{eq:lagrange_1_s},\\
T&{}={}&s^2J_2(\theta_1 + \theta_2)+eM_2[{g+(e-r)s^2}\theta_1\nonumber \\
 &&+(g+es^2)\theta_2] \label{eq: lagrange_2_s}.
\end{eqnarray}
Poprzez eliminacje z równań (\ref{eq:lagrange_1}) oraz (\ref{eq: lagrange_2_s}) $T$ otrzymujemy transmitancje kuli z wahadłem:
\setlength{\arraycolsep}{0.0em}
\begin{eqnarray}
\frac{\theta_1}{\theta_2}&{}={}&\frac{-2s^2J_2+e(-2g+(-2e+r)s^2)M_2}{-sT_v+2egM_2+s^2A} \label{eq:transfer_function}
\end{eqnarray}
gdzie:\\
$A=J_1+2J2+r^2M_1+(2e^2-3er+r^2)M_2$.\\
Model silnika DC otrzymujemy poprzez transformatę Laplace'a oraz przekształcenie równań (\ref{eq:motor_voltage}), (\ref{eq:motor_torque}) oraz ({\ref{eq:motor_noise}):
\setlength{\arraycolsep}{0.0em}
\begin{eqnarray}
I_t(s)&{}={}&\frac{1}{R_t+sL_t}U_t(s)-C_v\omega(s) \label{eq:motor_voltage_s}, \\
M_{e}&{}={}&c_MI_t(s) \label{eq:motor_torgue_s}, \\
\omega(s)&{}={}&\frac{1}{sJ_c}(M_{em}(s)-M_{obc}(s)) \label{eq:motor_noise_s}.
\end{eqnarray}
Schemat blokowy modelu silnika przedstawiono na rysunku \ref{fig:10_schemat_blokowy}.
\begin{figure}[!ht]
\captionsetup{justification=centering}
\centering
\includegraphics[width=3.5in]{10_schemat_blokowy}
\caption{Schemat blokowy modelu silnika DC.}
\label{fig:10_schemat_blokowy}
\end{figure}
Na podstawie otrzymanych zależności stworzono model obiektu w programie Simulink. Jako moment obciążenia silnika przyjęto moment pochodzący od siły tarcia oraz od siły grawitacji działającej na wahadło. 
\setlength{\arraycolsep}{0.0em}
\begin{eqnarray}
M_{obc}&{}={}&T_v\omega_1+egM_2sin(\theta_2) \label{eq:M_obc}
\end{eqnarray}
Transmitancja opisująca kulę z wahadłem została pomnożona przez promień kuli $r$, tak aby wielkością wyjściową było przemieszczenie robota. 
%tutututututututttttttttttttttttttttttttttttttttttttttttttttttt
\begin{figure*}[!ht]
\captionsetup{justification=centering}
\centering
\includegraphics[width=\textwidth]{13_model_reg}
\caption{Model układu regulacji robota kulistego z możliwością zadania wymuszenia skokowego lub wymuszenia liniowo narastającego.}
\label{fig:13_model_reg}
\end{figure*}
Podczas modelowania przyjęto następujące parametry obiektu: \\
$R_t=0,29\ \Omega$, $C_v=0,253\ \frac{Vs}{rad}$, $C_M=0,235\ \frac{Nm}{A}$, $J_c=0,0014\ kgm^2$, $r=0,226\ m$, $e=0,065\ m$, $J_1=0,0633\ kgm^2$, $J_2=0,007428\ kgm^2$, $M_1=3294\ g$, $M_2=1795\ g$, $g=9,81\ \frac{m}{s^2}$, $T_v = -0,1398\ \frac{Nm\cdot s}{rad}$.

Podczas symulacji działania obiektu zbadano odpowiedź układu na skokową oraz impulsową wartość zadaną.  
\begin{figure}[!ht]
\captionsetup{justification=centering}
\centering
\includegraphics[width=0.48\textwidth]{12_odpowiedz_2}
\caption{Odpowiedź obiektu na wymuszenie skokowe o amplitudzie 3.}
\label{fig:12_odpowiedz_2}
\end{figure}

\begin{figure}[!ht]
\captionsetup{justification=centering}
\centering
\includegraphics[width=0.48\textwidth]{09_odpowiedz}
\caption{Odpowiedź obiektu na wymuszenie impulsowe o amplitudzie 3 i czasie trwania impulsu 1s.}
\label{fig:09_odpowiedz}
\end{figure}
Zgodnie z przewidywaniami odpowiedź na impulsową wartość zadaną ma postać gasnących oscylacji (rysunek \ref{fig:09_odpowiedz}), natomiast na skokowe wymuszenie jest funkcją narastającą (zbliżoną do funkcji liniowej)(rysunek \ref{fig:12_odpowiedz_2}).

\section{Układ regulacji}

Sterowanie przemieszczeniem liniowym robota odbywać się będzie poprzez zmianę napięcia zasilania silnika DC znajdującego się wewnątrz kuli.

W celu zapewnienia możliwości wygodnego sterowania modelowanym robotem konieczne było zaimplementowanie układu regulacji robota kulistego. 
\begin{figure}[!ht]
\captionsetup{justification=centering}
\centering
\includegraphics[width=0.48\textwidth]{16_jakies_gowno}
\caption{Odpowiedź układu z regulatorem PID na wymuszenie skokowe o amplitudzie 3.}
\label{fig:16_jakies_gowno}
\end{figure}


Początkowo zbadano układ z regulatorem PID (gdzie: $K_p = 0,7 $, $K_i=0,574 $, $K_d=0,5 $) (rysunek \ref{fig:16_jakies_gowno}), jednak nie dawał on oczekiwanych rezultatów. Dlatego zdecydowano się na wykorzystanie regulatora zmiennych stanu.

Stanami układu będą położenie kątowe i prędkość kuli, a także jej przyspieszenie. Drugi i trzeci stan będą wyznaczane poprzez różniczkowanie kolejno jednokrotne i dwukrotne kąta wychylenia kuli będącego stanem pierwszym.

Parametrami zbudowanego regulatora (rysunek \ref{fig:13_model_reg}) są wzmocnienia poszczególnych stanów. Nastawy zostały dobrane doświadczalnie i wyniosły one $K_\alpha = 1,083$, $K_\omega = 0,48$ oraz $K_\epsilon = 0,5\cdot10^{-15}$.
\begin{figure}[!ht]
\captionsetup{justification=centering}
\centering
\includegraphics[width=0.48\textwidth]{14_odp_skok_reg_2}
\caption{Odpowiedź układu z regulatorem zmiennych stanu na wymuszenie skokowe o amplitudzie 3.}
\label{fig:14_odp_skok_reg_1}
\end{figure}
\begin{figure}[!ht]
\captionsetup{justification=centering}
\centering
\includegraphics[width=0.48\textwidth]{15_odp_zbocze_reg_2}
\caption{Odpowiedź układu z regulatorem zmiennych stanu na wymuszenie liniowo narastające.}
\label{fig:15_odp_zbocze_reg_1}
\end{figure}

Na podstawie uzyskanych przebiegów dla wymuszenia skokowego (rysunek \ref{fig:14_odp_skok_reg_1}) oraz dla wymuszenia liniowo narastającego (rysunek \ref{fig:15_odp_zbocze_reg_1}) można stwierdzić, że regulator działa w sposób zbliżony do poprawnego. Jednak w obu tych przebiegach występują odchyłki statyczne. Podjęto próbę ich wyeliminowania poprzez dodanie akcji całkującej na wyjściu z regulatora zmiennych stanu(rysunek \ref{fig:17_jakies_gowno}).
\begin{figure}[!ht]
\captionsetup{justification=centering}
\centering
\includegraphics[width=0.48\textwidth]{17_jakies_gowno}
\caption{Odpowiedź układu z regulatorem zmiennych stanu z akcją całkującą na wymuszenie skokowe o amplitudzie 3.}
\label{fig:17_jakies_gowno}
\end{figure}

\section{Wnioski}


Regulator PID pozwolił uzyskać zerową odchyłkę statyczną, jednak wystąpiło relatywnie duże przeregulowanie oraz długi czas regulacji ($24,9\ s$). Przejście na regulator zmiennych stanu  spowodowało znaczne skrócenie czasu regulacji($5,48\ s$), zmniejszenie przeregulowania. Niestety nie udało się zachować zerowej wartości odchyłki statycznej. Dodanie akcji całkującej pozwoliło zniwelować odchyłkę statyczną do zera, jednak spowodowało to znaczne wydłużenie czasu regulacji i zwiększenie przeregulowania. 

Na podstawie wykonanych badań uznano, że regulator zmiennych stanu bez akcji całkującej jest najlepszym rozwiązaniem w danych układzie. Jako kryterium wyboru przyjęto następujące wskaźniki jakości: czas regulacji oraz przeregulowanie. 


Jednak dla wymuszeń o wartościach większych niż 10, układ wchodził w bardzo duże oscylacje, a dobrane nastawy przestawały mieć sens. Być może inne wartości nastaw poprawiłby jego działanie, jednak ich dobór musiałby zostać wykonany inną metodą niż metoda doświadczalna.

\bibliographystyle{IEEEtran}
\bibliography{IEEEabrv,mybibfile}

\end{document}